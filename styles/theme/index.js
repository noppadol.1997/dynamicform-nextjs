import { createTheme } from "@mui/material/styles";
const theme = createTheme({
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          background: `#fff`,
        // baxkgroundColor:'#45D8A3',
          margin: "0 auto",
          WebkitFontSmoothing: "auto",
          // fontFamily: [
          //   "DB Heavent",
          //   "Work Sans",
          //   "-apple-system",
          //   "BlinkMacSystemFont",
          //   '"Segoe UI"',
          //   "Roboto",
          //   '"Helvetica Neue"',
          //   "Arial",
          //   "sans-serif",
          //   '"Apple Color Emoji"',
          //   '"Segoe UI Emoji"',
          //   '"Segoe UI Symbol"',
          //   "DBHeavent",
          // ].join(","),
          fontFamily: `"Roboto","Helvetica","Arial", sans-serif"`,
          //   fontSize: "25px",
          //   lineHeight: "25px",
          maxWidth: "1920px",
          backgroundColor:"khaki",
          // background: `linear-gradient(315deg,#41B0F5 20%, #45D8A3 80%)`,
          color: "#3d3d3d",
          overflowX: "hidden",
          MuiButton: {
            text: {
              color: "white",
            },
          },
          "@media (max-width: 1024px)": {
            backgroundSize: "70vw auto",
          },
          "@media (max-width: 767px)": {
            backgroundSize: "40% auto",
          },
        },
        ".MuiContainer-root": {
          padding: "0 15px !important",
        },
        ".MuiButtonBase-root.MuiButton-root": {
          minWidth: "200px",
          //   fontSize: "23px",
          //   lineHeight: "35px",
          boxShadow: "none",
        },
      },
    },
    MuiButtonBase: {},
  },
  palette: {
    primary: {
      /* button */
      main: "#1F419B",
      // contrastText: "#F5FF00",
    },
    secondary: {
      main: "#1688C4",
      contrastText: "#fff",
    },
    error: {
      main: "#f44336",
      contrastText: "#fff",
    },
    warning: {
      main: "#ff9800",
      contrastText: "#fff",
    },
    info: {
      main: "#2196f3",
      contrastText: "#fff",
    },
    success: {
      main: "#4caf50",
      contrastText: "#fff",
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 768,
      md: 1024,
      lg: 1280,
      xl: 1920,
    },
  },
});

export default theme;
