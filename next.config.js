/** @type {import('next').NextConfig} */
const nextConfig = {
  distDir: "build",
  reactStrictMode: true,
  styledComponents: true,
  experimental: {
    externalDir: true,
  },
}

module.exports = nextConfig
