import React from 'react';
import { CacheProvider } from '@emotion/react';
import { ThemeProvider, CssBaseline } from '@mui/material';
import PropTypes from 'prop-types';
import createEmotionCache from '@/utility/createEmotionCache';
import theme from '@/styles/theme';
import Layouts from '@/components/layout/layouts'


const clientSideEmotionCache = createEmotionCache();

const MyApp = (props) => {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  return (
    <CacheProvider value={emotionCache}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Layouts>
        <Component {...pageProps} />
        </Layouts>
      </ThemeProvider>
    </CacheProvider>
  );
};

export default MyApp
MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  emotionCache: PropTypes.object,
  pageProps: PropTypes.object.isRequired,
};
