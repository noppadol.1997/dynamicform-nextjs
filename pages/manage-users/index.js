import { useState } from "react";
import {
  Box,
  Table,
  TableHead,
  TableContainer,
  TableCell,
  TableBody,
  Paper,
  TableRow,
  Button,
} from "@mui/material";
import { useRouter } from "next/router";
import TableCustom from "@/components/tableCustom";

const rows = [
  {
    name: "test1",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
  {
    name: "test2",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
  {
    name: "test3",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
  {
    name: "test4",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
  {
    name: "test5",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
  {
    name: "test6",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
  {
    name: "test7",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
  {
    name: "test8",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
  {
    name: "test9",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
  {
    name: "test10",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
  {
    name: "test11",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
  {
    name: "test12",
    lastname: "testLastName1",
    email: "testmail@mail.com",
    status: "Active",
    create: "11/01/2023",
    update: "11/01/2023",
  },
];

let tableHeader = [
  { name: "Name" },
  { name: "Lastname" },
  { name: "Email" },
  { name: "Status" },
  { name: "Created" },
  { name: "Updated" },
];

const ManageUser = () => {
  const router = useRouter();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [formSearch, setFormSearch] = useState({
    name:'',
    email:''
  })

  const handleResetSearch = () =>{
    setFormSearch({
      name:'',
      email:''
    })
  }

  const handleSearch = ()=>{
    console.log("=====search======")
  }

  const handleCreateUser = () => {
    router.push(
      {
        pathname: "/manage-users/create-user",
        query: { namePage: "Create User" },
      },
      "/manage-users/create-user"
    );
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const onChangeField1 = (e) =>{
    setFormSearch({...formSearch,name:e.target.value})
  }

  const onChangeField2 = (e) =>{
    setFormSearch({...formSearch,email:e.target.value})
  }

  

  return (
    <Box>
      <Button
        sx={{ marginBottom: "20px" }}
        variant="contained"
        onClick={handleCreateUser}
      >
        create user
      </Button>
      <TableCustom
        tableHeader={tableHeader}
        count={rows.length}
        page={page}
        value1={formSearch.name}
        onChangeField1={onChangeField1}
        value2={formSearch.email}
        onChangeField2={onChangeField2}
        handleSearch={handleSearch}
        handleReset={handleResetSearch}
        rowsPerPage={rowsPerPage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        onPageChange={handleChangePage}
      >
        {(rowsPerPage > 0
          ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
          : rows
        ).map((row,index) => (
          <TableRow
            key={`${row.name}-${index}`}
            sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
          >
            <TableCell component="th" scope="row">
              {row.name}
            </TableCell>
            <TableCell align="left">{row.lastname}</TableCell>
            <TableCell align="left">{row.email}</TableCell>
            <TableCell align="left">{row.status}</TableCell>
            <TableCell align="left">{row.create}</TableCell>
            <TableCell align="left">{row.update}</TableCell>
          </TableRow>
        ))}
      </TableCustom>
    </Box>
  );
};

export default ManageUser;
