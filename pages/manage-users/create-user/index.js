import { useState } from "react";
import { Box, Typography } from "@mui/material";
import { useRouter } from "next/router";
import { makeStyles } from "@mui/styles";
import PaperCustom from "@/components/paperCustom";
import TextFieldCustom from "@/components/textCustom";
import ButtonCustom from "@/components/buttonCustom";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    width: "100%",
    alignItems: "center",
    flexDirection: "column",
  },
  titleForm: {},
}));
const CreateUser = () => {
  const classes = useStyles();
  const [formSubmit, setFormSubmit] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
  });

  const handleChange = (props) => {
    const { name, value } = props.target;
    setFormSubmit({ ...formSubmit, [name]: value });
  };

  return (
    <PaperCustom>
      <Box className={classes.root}>
        <Box>
          <Typography variant="h5" className={classes.titleForm}>
            Create User
          </Typography>
          <TextFieldCustom
            name="firstName"
            fieldName="Firstname"
            onChange={handleChange}
            value={formSubmit.firstName}
          />
          <TextFieldCustom
            name="lastName"
            fieldName="Lastname"
            onChange={handleChange}
            value={formSubmit.lastName}
          />
          <TextFieldCustom
            name="email"
            fieldName="Email"
            type="email"
            onChange={handleChange}
            value={formSubmit.email}
          />
          <TextFieldCustom
            name="password"
            fieldName="Password"
            type="password"
            onChange={handleChange}
            value={formSubmit.password}
          />
          <TextFieldCustom
            name="confirmPassword"
            fieldName="Confirm password"
            type="password"
            onChange={handleChange}
            value={formSubmit.confirmPassword}
          />
        </Box>
        <ButtonCustom title="Submit" />
      </Box>
    </PaperCustom>
  );
};

export default CreateUser;
