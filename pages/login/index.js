import {
  Box,
  Typography,
  Container,
  TextField,
  Button,
  Grid,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useRouter } from "next/router";
import React from "react";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
  },
  fontLogin: {
    fontSize: "30px",
    fontWeight: "bold",
    marginBottom:'60px'
  },
  boxLogin: {
    width: "400px",
    top: "50%",
    left: "50%",
    transform: "translate(-50%,-50%)",
    position:'fixed',
    maxWidth: "400px",
    height: "500px",
        justifyContent: "center",
    backgroundColor: "#fff",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  boxTextField:{
    width: "inherit",
    display:'grid',
    justifyContent:'center'
  },
  boxButton:{
    marginTop:'40px'
  }
}));
const Login = () => {
  const classes = useStyles();
  const router = useRouter()
  const handleSubmit = ()=>{
    router.push({
      pathname: '/dashboard',
      query: { namePage: "Dashboard" },
    },'/dashboard')
  }

  return (
    <Box className={classes.root}>
<Container maxWidth="lg">
        <Box className={classes.boxLogin}>
          <Typography className={classes.fontLogin}>Login</Typography>
          <Grid className={classes.boxTextField} container spacing={2}>
            <Grid item xs={12}>
              <TextField />
            </Grid>
            <Grid item xs={12}>
              <TextField />
            </Grid>
          </Grid>
          <Button className={classes.boxButton} onClick={handleSubmit}>Login</Button>
        </Box>
    </Container>
    </Box>
    
  );
};

export default Login;
