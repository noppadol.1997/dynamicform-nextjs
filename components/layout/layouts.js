import * as React from "react";
import { styled, useTheme } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import MuiAppBar from "@mui/material/AppBar";
import {
  Divider,
  IconButton,
  Box,
  Container,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
  List,
  Toolbar,
  Drawer,
} from "@mui/material";
import Link from "@/src/Links";
import { useRouter } from "next/router";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import ContactEmergencyIcon from "@mui/icons-material/ContactEmergency";

const drawerWidth = 240;

const Main = styled("main", { shouldForwardProp: (prop) => prop !== "open" })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  })
);

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  transition: theme.transitions.create(["margin", "width"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
  justifyContent: "flex-end",
}));

const listItemPage = [
  {
    name: "Manage Users",
    path: "/manage-users",
    icon: <ContactEmergencyIcon />,
  },
  { name: "Logout", path: "/login", icon: <ExitToAppIcon /> },
];

const Layouts = (props) => {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [headerTitle, setHeaderTitle] = React.useState("");

  const { children } = props;
  const router = useRouter();
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  React.useEffect(() => {
    if (router?.query?.namePage) {
      setHeaderTitle(router.query.namePage);
    }
  }, [router]);

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      {router.pathname === "/login" ? (
        <Main>
          <Container maxWidth="xl">{children}</Container>
        </Main>
      ) : (
        <>
          <AppBar position="fixed" open={open}>
            <Toolbar>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={handleDrawerOpen}
                edge="start"
                sx={{ mr: 2, ...(open && { display: "none" }) }}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" noWrap component="div">
                {headerTitle || ""}
              </Typography>
            </Toolbar>
          </AppBar>
          <Drawer
            sx={{
              width: drawerWidth,
              flexShrink: 0,
              "& .MuiDrawer-paper": {
                width: drawerWidth,
                boxSizing: "border-box",
              },
            }}
            variant="persistent"
            anchor="left"
            open={open}
          >
            <DrawerHeader>
              <IconButton onClick={handleDrawerClose}>
                {theme.direction === "ltr" ? (
                  <ChevronLeftIcon />
                ) : (
                  <ChevronRightIcon />
                )}
              </IconButton>
            </DrawerHeader>
            <Divider />
            <List>
              {listItemPage.map((text, index) => (
                <Link
                  href={text.path}
                  style={{ textDecoration: "none", color: "black" }}
                  key={index}
                  onClick={() => setHeaderTitle(text.name)}
                >
                  <ListItem key={text.name} disablePadding>
                    <ListItemButton>
                      <ListItemIcon>{text.icon}</ListItemIcon>
                      <ListItemText primary={text.name} />
                    </ListItemButton>
                  </ListItem>
                </Link>
              ))}
            </List>
          </Drawer>
          <Main open={open}>
            <DrawerHeader />
            <Container maxWidth="xl">{children}</Container>
          </Main>
        </>
      )}
    </Box>
  );
};

export default Layouts;
