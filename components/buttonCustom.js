import { makeStyles } from "@mui/styles";
import { Button, Box, TextField, Typography } from "@mui/material";

const useStyles = makeStyles(() => ({
  roots: {
    marginTop: "30px",
    maxWidth: "250px",
    width: "180px",
  },
}));

const ButtonCustom = (props) => {
  const classes = useStyles();
  return (
    <Box className={classes.roots}>
      <Button variant="contained" sx={{ maxWidth: "250px", width: "180px" ,height:'50px'}}>
        {props.title || "Enter"}
      </Button>
    </Box>
  );
};

export default ButtonCustom;
