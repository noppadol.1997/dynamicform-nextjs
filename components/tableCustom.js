import {
  Box,
  Table,
  TableHead,
  TableContainer,
  TableCell,
  TableBody,
  Paper,
  TableRow,
  Button,
  TablePagination,
  TableFooter,
  TextField,
  Grid,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useState } from "react";
import { useTheme } from "@mui/material/styles";
import IconButton from "@mui/material/IconButton";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";

const useStyles = makeStyles(() => ({
  roots: {},
}));

const TablePaginationActions = (props) => {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5 }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
};

const TableCustom = (props) => {
  const classes = useStyles();

  const renderHeaderTable = (prop) => {
    let tableHeader = prop;
    if (tableHeader?.length > 0) {
      return tableHeader.map((items, index) => (
        <TableCell key={index}>{items?.name}</TableCell>
      ));
    }
  };

  return (
    <>
    {props.childrenHeader ? (
        props.childrenHeader
      ) : (
        <Grid
          container
          spacing={2}
          sx={{ marginBottom: "15px", marginTop: "15px" }}
        >
          <Grid item xs={12} sm={4}>
            <TextField
              InputLabelProps={{ shrink: true }}
              label="Name"
              value={props.value1}
              onChange={props.onChangeField1}
              sx={{ minWidth: "-webkit-fill-available" }}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <TextField
              InputLabelProps={{ shrink: true }}
              label="Email"
              value={props.value2}
              onChange={props.onChangeField2}
              sx={{ minWidth: "-webkit-fill-available" }}
            />
          </Grid>
          <Grid item xs={12} sm={2}>
            <Button
              variant="contained"
              onClick={props.handleSearch}
              sx={{ minWidth: "-webkit-fill-available", minHeight: "100%" }}
            >
              Search
            </Button>
          </Grid>
          <Grid item xs={12} sm={2}>
            <Button
              variant="contained"
              onClick={props.handleReset}
              sx={{ minWidth: "-webkit-fill-available", minHeight: "100%" }}
            >
              Reset
            </Button>
          </Grid>
        </Grid>
      )}
      <TableContainer sx={{ marginTop: "15px" }} component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="custom pagination table">
        <TableHead>
          <TableRow>{renderHeaderTable(props.tableHeader)}</TableRow>
        </TableHead>
        <TableBody>{props.children}</TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[
                10,
                25,
                50,
                100,
                { label: "All", value: -1 },
              ]}
              count={props.count}
              rowsPerPage={props.rowsPerPage}
              page={props.page}
              SelectProps={{
                inputProps: {
                  "aria-label": "rows per page",
                },
                native: true,
              }}
              onPageChange={props.onPageChange}
              onRowsPerPageChange={props.onRowsPerPageChange}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
    </>
    
  );
};

export default TableCustom;
