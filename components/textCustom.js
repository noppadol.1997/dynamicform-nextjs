import { makeStyles } from "@mui/styles";
import { Paper,Box,TextField,Typography } from "@mui/material";

const useStyles = makeStyles(()=>({
    roots:{
        marginTop:'10px'
    },
    bodyContent:{
        display:'flex',
        padding:'20px'
    }
}))

const TextFieldCustom =(props)=>{
    const classes = useStyles()
    return (
        <Box className={classes.roots}>
        <Typography  sx={{marginBottom:'5px'}}>{props.fieldName}</Typography>
        <TextField name={props.name} type={props.type} value={props.value} onChange={props.onChange}/>
        </Box>
    )
}

export default TextFieldCustom