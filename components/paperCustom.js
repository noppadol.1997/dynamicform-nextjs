import { makeStyles } from "@mui/styles";
import { Paper,Box } from "@mui/material";

const useStyles = makeStyles(()=>({
    root:{
    },
    bodyContent:{
        display:'flex',
        padding:'40px 20px'
    }
}))
const PaperCustom = (props)=>{
    const classes = useStyles()
 return (
    <Paper  elevation={6} >
        <Box className={classes.bodyContent}>{props.children}</Box>
    </Paper>
 )
}

export default PaperCustom